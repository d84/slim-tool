<?php
namespace d84\SlimCli;

require __DIR__ . '/../vendor/autoload.php';
require 'RouteAnnotation.php';

use Doctrine\Common\Annotations\AnnotationReader;

$reader = new AnnotationReader();

/**
 * @RouteAnnotation(path="/api/v1")
 */
class Controller
{
    /**
     * @RouteAnnotation(path="/test")
     */
    public function test()
    {
    }

    /**
     * @RouteAnnotation(path="/users/list")
     */
    public function listUsers()
    {
    }
}

$cls = new \ReflectionClass(Controller::class);
$route_annotation = $reader->getClassAnnotation($cls, RouteAnnotation::class);
$methods = $cls->getMethods(\ReflectionMethod::IS_PUBLIC);

$base_path = $route_annotation->path;

foreach ($methods as $m) {
    $classAnnotations = $reader->getMethodAnnotations($m);

    foreach ($classAnnotations as $annot) {
        print $base_path . $annot->path . "\r\n";
    }
}
