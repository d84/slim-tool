<?php
namespace d84\SlimCli\Template;

/**
 * @var TemplateFactory
 */
abstract class TemplateFactory
{
    public static function create(string $name, array $config = []): TemplateInterface
    {
        $tpl_class = 'd84\SlimCli\Template\\' . ucfirst(strtolower($name));
        if (!class_exists($tpl_class)) {
            throw new \RuntimeException("Couldn't found template engine with name '$name'");
        }
        return $tpl_class::factory($config);
    }
}
