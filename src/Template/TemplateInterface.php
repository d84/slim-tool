<?php
namespace d84\SlimCli\Template;

/**
 * @var TemplateInterface
 */
interface TemplateInterface
{
    /**
     * @param  string $template
     * @param  array  $scope
     * @return string
     */
    public function render(string $template, array $scope): string;
}
