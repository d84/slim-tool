<?php
namespace d84\SlimCli\Template\Extensions\Twig;

/**
 * @var DoubleBackslash
 */
final class DoubleBackslash extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
          new \Twig_SimpleFilter('doublebackslash', [$this, 'doublebackslash']),
        ];
    }

    /**
     * @param  string $string
     * @return string
     */
    public function doublebackslash(string $string)
    {
        return str_replace('\\', '\\\\', $string);
    }
}
