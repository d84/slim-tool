<?php
namespace d84\SlimCli\Template\Extensions\Twig;

/**
 * @var DocBlock2Html
 */
final class DocBlock2Html extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
          new \Twig_SimpleFilter('docblock2html', [$this, 'docblock2html']),
        ];
    }

    /**
     * @param  string $string
     * @return string
     */
    public function docblock2html(string $string)
    {
        $search  = '/(\s*@\w+)/';
        $replace = '<span class="badge badge-warning font-weight-bold p-1 m-1">$1</span>';
        $lines   = explode("\n", $string);
        $text    = [];

        foreach ($lines as $line) {
            $line = trim($line);
            if ($line === '/**' || $line === '*/' || $line === '*' || strlen($line) == 0) {
                continue;
            }
            $line = preg_replace($search, $replace, $line);
            if ($line[0] === '*') {
                $line = substr($line, 1);
            }

            // TODO: Проблема с подсвечиванием подстрок в строках
            // Пример - Print , будет подсвечен как Pr<b>int</b>
            /*$line = str_replace(
              [ 'string', 'int',
                'float', 'bool',
                'boolean', 'null',
                'mixed'],
              [ '<b>string</b>', '<b>int</b>',
                '<b>float</b>', '<b>bool</b>',
                '<b>boolean</b>', '<b>null</b>',
                '<b>mixed</b>'],
              $line
            );*/
            $text[] = trim($line);
        }
        
        return trim(implode('<br>', $text));
    }
}
