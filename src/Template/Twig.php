<?php
namespace d84\SlimCli\Template;

use d84\SlimCli\Helper\ArrayHelper;

/**
 * @var Twig
 */
final class Twig implements TemplateInterface
{
    /**
     * @var \Twig_Environment
     */
    private $engine;

    /**
     * @param string $template_path
     * @param bool   $debug
     * @param array  $extensions
     */
    public function __construct(
      string $template_path,
      bool $debug = false,
      array $extensions = [],
      array $functions = []
    ) {
        $loader = new \Twig_Loader_Filesystem($template_path);
        $this->engine = new \Twig_Environment($loader, ['debug' => $debug]);

        // DEBUG MODE!
        if ($debug) {
            $this->engine->addExtension(new \Twig_Extension_Debug());
        }

        // Add extensions
        foreach ($extensions as $ext) {
            if (is_string($ext)) {
                $this->engine->addExtension(new $ext);
            } else {
                $this->engine->addExtension($ext);
            }
        }

        // Add Functions
        foreach ($functions as $name => $callable) {
            $this->engine->addFunction(new \Twig_SimpleFunction($name, $callable));
        }
    }

    /**
     * @param  string $template
     * @param  array  $scope
     * @return string
     */
    public function render(string $template, array $scope): string
    {
        $tpl = $this->engine->load($template);
        return trim($tpl->render($scope));
    }

    /**
     * @param  array  $config
     * @return self
     */
    public static function factory(array $config)
    {
        $tpl_path   = ArrayHelper::extractStr('template_path', $config, null, true);
        $debug      = ArrayHelper::extractBool('debug', $config, false);
        $extensions = ArrayHelper::extractArray('extensions', $config, []);
        $functions  = ArrayHelper::extractArray('functions', $config, []);
        return new Twig($tpl_path, $debug, $extensions, $functions);
    }
}
