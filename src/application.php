<?php
//\Phar::mount('/Assets/Template', dirname(\Phar::running(false)) .'/build/target/Assets/Template');

define('PHAR_NAME', 'phar://slim-tool.phar');
require PHAR_NAME . '/build/target/vendor/autoload.php';

use Symfony\Component\Console\Application;

$config = [
  'logger' => [
    'rotating' => [
      'filename'  => 'slimtool.log',
      'max_files' => 10,
      'level'     => 'DEBUG',
      'format' => [
        'type'                           => 'lineformat',
        'format'                         => "[%datetime%] %channel%.%level_name%: %message%\n",
        'ignore_empty_context_and_extra' => true
      ]
    ]
  ]
];

$application = new Application();
$application->add(new d84\SlimCli\Command\Help($config));
$application->add(new d84\SlimCli\Command\Route\Map($config));
$application->run();
