<?php
namespace d84\SlimCli\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @var Help
 */
class Help extends AbstractCommand
{
    public function __construct(array $config)
    {
        parent::__construct($config);
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this->setName('help')->setDescription('Display help');
    }

    /**
     * @param  InputInterface  $input
     * @param  OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Help me');
        $this->logger->error('Help me!');
    }
}
