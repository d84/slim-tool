<?php
namespace d84\SlimCli\Command;

use Symfony\Component\Console\Command\Command;
use d84\SlimCli\Logger\LoggerFactory;
use Psr\Log\LoggerInterface;

/**
 * @var AbstractCommand
 */
abstract class AbstractCommand extends Command
{
    /** @var LoggerInterface */
    protected $logger;

    public function __construct(array $config)
    {
        parent::__construct();
        $logger_config = $config['logger'] ?? [];
        $logger_config = array_merge($logger_config, ['name' => get_called_class()]);
        $this->logger = LoggerFactory::create($logger_config);
    }
}
