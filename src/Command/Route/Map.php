<?php
namespace d84\SlimCli\Command\Route;

use d84\SlimCli\Template\Twig;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Filesystem\Filesystem;

use Doctrine\Common\Annotations\AnnotationReader;

use d84\SlimCli\Command\AbstractCommand;
use d84\SlimCli\Template\TemplateFactory;
use d84\SlimCli\Template\Extensions\Twig\DoubleBackslash;
use d84\SlimCli\Template\Extensions\Twig\DocBlock2Html;

use d84\SlimCli\Annotation\ControllerProcessor;

use d84\SlimCli\Annotation\Scanner;

/**
 * Generates routes mapping in different formats.
 * Supported formats:
 *  + php - the map will be printed in Slim3 format
 *  + html
 *  + md
 */
class Map extends AbstractCommand
{
    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        parent::__construct($config);
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this->setName('route:map');
        $this->setDescription('Generate routes map');

        $this->addArgument(
          'scan_dir',
          InputArgument::OPTIONAL,
          'Path to the directory that will be scanned',
          'src' . DIRECTORY_SEPARATOR
        );

        $this->addOption('format', 'f', InputOption::VALUE_OPTIONAL, 'Output format: php,html,md', 'html');
        //$this->addOption('config', 'c', InputOption::VALUE_OPTIONAL, 'Configuration file');
        $this->addOption('debug', 'd', InputOption::VALUE_OPTIONAL, 'Debug mode', false);
        $this->addOption('output', 'o', InputOption::VALUE_OPTIONAL, 'Output directory', 'doc');
        $this->addOption('reference', 'r', InputOption::VALUE_OPTIONAL, 'Application reference [PHP]', '$app->');
        $this->addOption('zip', 'z', InputOption::VALUE_OPTIONAL, 'Remove empty lines from output', false);
    }

    /**
     * @param  InputInterface  $input
     * @param  OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->warning("----------------------------------------------");
        $this->logger->warning("BEGIN");

        $scan_dir   = str_replace('/', DIRECTORY_SEPARATOR, $input->getArgument('scan_dir'));
        $out_format = $input->getOption('format');
        $debug_mode = $input->getOption('debug');
        $out_dir    = $input->getOption('output');
        $app_ref    = $input->getOption('reference');
        $zip        = $input->getOption('zip');
        $autoload   = getcwd() . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

        $fs         = new Filesystem();
        $scanner    = new Scanner($autoload, new ControllerProcessor(new AnnotationReader(), $this->logger));

        $startup_args = "scan_dir=$scan_dir,"
                       ."out_dir=$out_dir,"
                       ."out_format=$out_format,"
                       ."app_ref=$app_ref,"
                       ."zip=$zip,"
                       ."autoload=$autoload";
        $output->writeln($startup_args);
        $this->logger->warning($startup_args);

        /*
          Scan annotations in dir
         */
        $output->writeln("Scan '$scan_dir'...");
        $scan_result = $scanner->scan($scan_dir);

        $output->writeln("Total files:     " . $scan_result['report']['total']);
        $output->writeln("Processed files: " . $scan_result['report']['processed']);

        $this->logger->warning(json_encode($scan_result['map'], JSON_PRETTY_PRINT));

        /*
          Generate some output
         */
        $output->writeln("Generate output...");

        $template = TemplateFactory::create(
          'twig',
          [
            'template_path' => PHAR_NAME . '/build/target/application/assets/template/route',
            'debug'         => $debug_mode,
            'extensions'    => [
              new DoubleBackslash(),
              new DocBlock2Html()
            ],
            'functions' => [
              'print_r'     => [$this, 'printr'],
              'print_rhtml' => [$this, 'printrhtml'],
              'tagtext'     => [$this, 'tagtext']
            ]
          ]
        );

        $result = $template->render(
          'map.twig',
          [
            'format'  => $out_format,
            'app_ref' => $app_ref,
            'map'     => $scan_result['map']
          ]
        );

        $result = $this->trimString($result);
        if ($zip) {
            $result = $this->zip($result);
        }

        $fs->dumpFile("$out_dir/routes.$out_format", $result);

        /**
         * Copy assets
         */
        if ($out_format === 'html') {
            $target = $out_dir;
            $source = [
              PHAR_NAME . '/build/target/vendor/twbs/bootstrap/dist',
              PHAR_NAME . '/build/target/vendor/twbs/bootstrap/assets'
            ];
            foreach ($source as $s) {
                $this->copydirr($s, $target, $fs);
                $this->copydirr($s, $target, $fs);
            }
        }

        $output->writeln("Done!");

        $this->logger->warning("END");
        $this->logger->warning("----------------------------------------------");
        $this->logger->warning("");
    }

    private function zip(string $text)
    {
        $lines = explode("\n", $text);
        $out = [];
        foreach ($lines as $line) {
            $line = trim($line);
            if (!empty($line)) {
                $out[] = $line;
            }
        }
        return implode("\n", $out);
    }

    /**
     * @param  string $str
     * @return string
     */
    private function resolveString(string $str)
    {
        $p = strpos($str, 'constant:');
        if ($p === 0) {
            return substr($str, strlen('constant:'));
        }
        return '"' . $str . '"';
    }

    // TODO: It must be placed in Tempalte/Extensions/Twig
    public function printr(?array $arr, int $level = 0)
    {
        if (is_null($arr)) {
            return '';
        }
        $out = [];
        foreach ($arr as $k => $v) {
            $row = '';
            if (is_numeric($k)) {
                //$row .= $k . '=>';
                // Number index do not print
            } else {
                $row .= '"' . $k . '"=>';
            }
            if (is_array($v)) {
                $row .= $this->printr($v, $level + 1);
            } elseif (is_string($v)) {
                $row .= $this->resolveString($v);
            } elseif (is_bool($v)) {
                $row .= $v === true ? 'true' : 'false';
            } elseif (is_null($v)) {
                $row .= 'null';
            } else {
                $row .= $v;
            }
            $out[] = $row;
        }
        if ($level === 0) {
            return implode(',', $out);
        }
        return '[' . implode(',', $out) . ']';
    }

    // TODO: It must be placed in Tempalte/Extensions/Twig
    public function printrhtml(?array $arr)
    {
        if (is_null($arr)) {
            return '';
        }
        if (empty($arr)) {
            return '[]';
        }
        $out = [];
        foreach ($arr as $k => $v) {
            $row = '';
            if (is_numeric($k)) {
                // Number index do not print
            } else {
                $row .= '"' . $k . '"=';
            }
            if (is_array($v)) {
                if (empty($v)) {
                    $row .= '[]';
                } else {
                    $row .= $this->printrhtml($v);
                }
            } elseif (is_string($v)) {
                $row .= $this->resolveString($v);
            } elseif (is_bool($v)) {
                $row .= $v === true ? 'true' : 'false';
            } elseif (is_null($v)) {
                $row .= 'null';
            } else {
                $row .= $v;
            }
            $out[] = '<li>' . $row . '</li>';
        }
        return '<ul>' . implode('', $out) . '</ul>';
    }

    /**
     * @param  string $text
     * @return string
     */
    public function tagtext(string $text)
    {
        $tags = ['details'];

        foreach ($tags as $tag) {
            $subtext = $this->gettagtext($text, $tag);
            if ($subtext !== null) {
                return $subtext;
            }
        }

        return '';
    }

    private function gettagtext(string $text, string $tag)
    {
        $ps  = strpos($text, "<$tag>");
        $pe  = strpos($text, "</$tag>");
        $len = strlen($tag);

        if ($ps !== false && $pe !== false) {
            $subtext = substr($text, $ps + $len + 2, $pe - ($ps + $len + 2));
            $subtext = explode("\n", $subtext);
            $out = [];
            foreach ($subtext as $s) {
                $s = trim($s);
                if (!empty($s) && $s[0] === '*') {
                    $s = trim(substr($s, 1));
                }
                $out[] = $s;
            }
            return implode("<br/>", $out);
        }

        return null;
    }

    /**
     * Copy a dirrectory recursive
     * @param  string     $src
     * @param  string     $dest
     * @param  Filesystem $fs
     * @return void
     */
    private function copydirr($src, $dest, Filesystem $fs)
    {
        if (!is_dir($dest)) {
            $fs->mkdir($dest);
        }

        $i = new \DirectoryIterator($src);
        foreach ($i as $f) {
            if ($f->isDot() || empty($f->getFileName())) {
                continue;
            }

            if ($f->isFile()) {
                $from = $f->getPath() .DIRECTORY_SEPARATOR . $f->getFilename();
                $to   = $dest . DIRECTORY_SEPARATOR . $f->getFilename();
                $fs->copy($from, $to);
            } elseif ($f->isDir()) {
                $this->copydirr($src . DIRECTORY_SEPARATOR . $f->getFileName(), $dest . DIRECTORY_SEPARATOR . $f, $fs);
            }
        }
    }

    /**
     * @param  string $s
     * @return string
     */
    private function trimString(string $s): string
    {
        // Replace multi whitespaces to single white space
        return preg_replace('! +!', ' ', $s);
    }
}
