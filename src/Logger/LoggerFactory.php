<?php
namespace d84\SlimCli\Logger;

use RuntimeException;
use Psr\Log\LoggerInterface;

/**
 * @var LoggerFactory
 */
abstract class LoggerFactory
{
    /**
     * @param  array  $config
     * @param  string $adapter
     *
     * @return LoggerInterface
     *
     * @throws RuntimeException
     */
    public static function create(array $config, string $adapter = 'monolog')
    {
        $adapter_service = 'd84\SlimCli\Logger\\'. ucfirst($adapter) . 'Logger';

        if (!class_exists($adapter_service)) {
            throw new RuntimeException("Not found adapter '$adapter'");
        }

        return $adapter_service::factory($config);
    }
}
