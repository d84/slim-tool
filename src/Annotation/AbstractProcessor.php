<?php
namespace d84\SlimCli\Annotation;

use Psr\Log\LoggerInterface;
use Doctrine\Common\Annotations\AnnotationReader;
use d84\SlimCli\Helper\ArrayHelper;

abstract class AbstractProcessor
{
    /** @var AnnotationReader */
    protected $reader;

    /** @var LoggerInterface */
    protected $logger;

    public function __construct(AnnotationReader $reader, LoggerInterface $logger)
    {
        $this->reader = $reader;
        $this->logger = $logger;
    }

    /**
     * @param array $annotations
     * @return array|bool
     */
    abstract protected function doProcessClassAnnotations(array $annotations);

    /**
     * @param array $annotations
     * @return array|bool
     */
    abstract protected function doProcessMethodAnnotations(array $annotations);

    /**
     * @param array $annotations
     * @return array|bool
     */
    abstract protected function doProcessPropertyAnnotations(array $annotations);

    /**
     * @param  string $file
     * @param  array  $filters []
     *                          ['class']
     *                            ['annotations']  array The list of class annotations to be processed
     *                          ['method']
     *                            ['annotations']  array The list of method annotations to be processed
     *                            ['names']        array The list of method names to be processed
     *                          ['property']
     *                            ['annotations']  array The list of property annotations to be processed
     *                            ['names']        array The list of property names to be processed
     * @return array|bool
     */
    public function process(string $file, array $filters = [])
    {
        $file_name    = basename($file);
        $file_content = file_get_contents($file);
        $class_name   = str_replace('.php', '', $file_name);
        $relative_path= str_replace(getcwd(), '', $file);

        $class_annotation_filters  = $this->getFilters('class/annotations', $filters);
        $method_annotation_filters = $this->getFilters('method/annotations', $filters);
        $method_names_filters      = $this->getFilters('method/names', $filters);
        $prop_annotation_filters   = $this->getFilters('property/annotations', $filters);
        $prop_names_filters        = $this->getFilters('property/names', $filters);

        $this->logger->info('process: (name=' . $file_name . ',class=' . $class_name . ')');

        if (!$this->hasClassDeclaration($file_content, $class_name)) {
            $this->logger->debug("process: Not found class '$class_name' declaration in '$file', stop processing!");
            return false;
        }

        include_once($file);

        try {
            $reflection = new \ReflectionClass($this->getNamespace($file_content) . '\\' . $class_name);

            /** @var \ReflectionMethod[] $methods */
            $methods = $reflection->getMethods(\ReflectionMethod::IS_PUBLIC);

            /** @var \ReflectionProperty[] $properties */
            $properties = $reflection->getProperties(
              \ReflectionProperty::IS_PUBLIC |
              \ReflectionProperty::IS_PROTECTED |
              \ReflectionProperty::IS_PRIVATE
            );
        } catch (\Exception $e) {
            $this->logger->debug("process: " . $e->getMessage());
            return false;
        }

        // ------------------------------------------------
        // Process class
        // ------------------------------------------------
        $class_result = $this->processClass($reflection, $class_annotation_filters);

        if ($class_result === false) {
            $this->logger->debug("process: processClass[{$reflection->getName()}]=false, stop rpcessing!");
            return false;
        }

        // ------------------------------------------------
        // Process methods
        // ------------------------------------------------
        $method_results = $this->processMethods($methods, $method_names_filters, $prop_annotation_filters);

        if ($method_results === false) {
            $this->logger->debug("process: processMethods=false, stop rpcessing!");
            return false;
        }

        // ------------------------------------------------
        // Process properties
        // ------------------------------------------------
        $property_results = $this->processProperties($properties, $prop_names_filters, $prop_annotation_filters);

        if ($property_results === false) {
            $this->logger->debug("process: processProperties=false, stop rpcessing!");
            return false;
        }

        $this->logger->debug("process: '$file_name' has been processed");

        return [
          'relative_path' => $relative_path,
          'class'         => $class_result,
          'properties'    => $property_results,
          'methods'       => $method_results
        ];
    }

    /**
     * @param  string $file_content
     * @return string
     */
    protected function getNamespace(string $file_content): string
    {
        if (preg_match('#^namespace\s+(.+?);$#sm', $file_content, $m)) {
            return $m[1];
        }
        return '';
    }

    /**
     * @param  string $file_content
     * @param  string $look_for_class
     * @return bool
     */
    protected function hasClassDeclaration(string $file_content, string $look_for_class): bool
    {
        if (preg_match("#class\s+$look_for_class#sm", $file_content, $m)) {
            return true;
        }
        return false;
    }

    /**
     * @param  array  $annotations
     * @param  string $class
     * @return mixed
     */
    protected function findFirst(array $annotations, string $class)
    {
        foreach ($annotations as $anno) {
            if ($anno instanceof $class) {
                return $anno;
            }
        }
        return null;
    }

    /**
     * @param  array  $annotations
     * @param  array  $classes
     * @return mixed
     */
    protected function findFirstOf(array $annotations, array $classes)
    {
        foreach ($annotations as $anno) {
            foreach ($classes as $class) {
                if ($anno instanceof $class) {
                    return $anno;
                }
            }
        }
        return null;
    }

    /**
     * @param  array  $annotations
     * @param  string $class
     * @return array
     */
    protected function findAll(array $annotations, string $class)
    {
        $out = [];
        foreach ($annotations as $anno) {
            if ($anno instanceof $class) {
                $out[] = $anno;
            }
        }
        return $out;
    }

    /**
     * @param  array  $annotations
     * @param  string $class
     * @param  array  $args
     * @return mixed
     */
    protected function ensureWithArgs(array $annotations, string $class, array $args)
    {
        $anno = $this->findFirst($annotations, $class);
        if (is_null($anno)) {
            $anno = new $class($args);
        }
        return $anno;
    }

    /**
     * @param  array  $annotations
     * @param  array  $filter      The list of class names
     * @return array
     */
    private function reduceAnnotationList(array $annotations, array $filter)
    {
        if (empty($filter)) {
            return $annotations;
        }

        $out = [];
        foreach ($filter as $annotation_class) {
            foreach ($annotations as $anno) {
                if ($anno instanceof $annotation_class) {
                    $out[] = $anno;
                }
            }
        }

        return $out;
    }

    /**
     * @param  \ReflectionClass $reflection
     * @param  array            $annotation_filters
     * @return array|bool
     */
    private function processClass(\ReflectionClass $reflection, array $annotation_filters)
    {
        $annotations = $this->reader->getClassAnnotations($reflection);

        $this->logger->debug(
          "processClass: " . $reflection->getName() . ": " . json_encode($annotations, JSON_PRETTY_PRINT)
        );

        $annotations = $this->reduceAnnotationList($annotations, $annotation_filters);

        $this->logger->debug("processClass: Reduced: " . json_encode($annotations, JSON_PRETTY_PRINT));

        return $this->doProcessClassAnnotations($annotations);
    }

    /**
     * @param  array  $reflections
     * @param  array  $names_filters
     * @param  array  $annotation_filters
     * @return array|bool
     */
    private function processMethods(array $reflections, array $names_filters, array $annotation_filters)
    {
        $method_results = [];

        foreach ($reflections as $reflection) {
            $m_name = $reflection->getName();

            if (!empty($names_filters) && !isset($names_filters[$m_name])) {
                $this->logger->warning("processMethods: Not found names_filters[$m_name], skip rpcessing!");
                continue;
            }

            $annotations = $this->reader->getMethodAnnotations($reflection);

            $this->logger->debug("processMethods: $m_name(): " . json_encode($annotations, JSON_PRETTY_PRINT));

            $annotations = $this->reduceAnnotationList($annotations, $annotation_filters);

            $this->logger->debug("processMethods: Reduced: " . json_encode($annotations, JSON_PRETTY_PRINT));

            $method_result = $this->doProcessMethodAnnotations($annotations);

            if ($method_result === false) {
                $this->logger->debug("processMethods: doProcessMethodAnnotations[$m_name]=false, stop rpcessing!");
                return false;
            }

            if (empty($method_result)) {
                $this->logger->warning(
                  "processMethods: doProcessMethodAnnotations[$m_name]=empty, not save the results!"
                );
                continue;
            }

            $method_results[$m_name] = [
              'annotations' => $method_result,
              'reflection'  => $reflection
            ];
        }

        return $method_results;
    }

    /**
     * @param  array  $reflections
     * @param  array  $names_filters
     * @param  array  $annotation_filters
     * @return array|bool
     */
    private function processProperties(array $reflections, array $names_filters, array $annotation_filters)
    {
        $property_results = [];

        foreach ($reflections as $reflection) {
            $p_name = $reflection->getName();

            if (!empty($names_filters) && !isset($names_filters[$p_name])) {
                $this->logger->warning("processProperties: Not found names_filters[$p_name], skip rpcessing!");
                continue;
            }

            $annotations = $this->reader->getPropertyAnnotations($reflection);

            $this->logger->debug("processProperties: $p_name: " . json_encode($annotations, JSON_PRETTY_PRINT));

            $annotations = $this->reduceAnnotationList($annotations, $annotation_filters);

            $this->logger->debug("processProperties: Reduced: " . json_encode($annotations, JSON_PRETTY_PRINT));

            $property_result = $this->doProcessPropertyAnnotations($annotations);

            if ($property_result === false) {
                $this->logger->debug("processProperties: doProcessPropertyAnnotations[$p_name]=flase, stop rpcessing!");
                return false;
            }

            if (empty($property_result)) {
                $this->logger->warning(
                  "processProperties: doProcessPropertyAnnotations[$p_name]=empty, not save the results!"
                );
                continue;
            }

            $property_results[$p_name] = [
              'annotations' => $property_result,
              'reflection'  => $reflection
            ];
        }

        return $property_results;
    }

    /**
     * @param  string $path
     * @param  array  $filters
     * @return array
     */
    private function getFilters(string $path, array $filters)
    {
        $filter = [];
        try {
            $filter = ArrayHelper::find($path, $filters);
        } catch (\RuntimeException $e) {
            $filter = [];
        }
        return $filter;
    }
}
