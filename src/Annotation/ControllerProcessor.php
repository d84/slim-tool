<?php
namespace d84\SlimCli\Annotation;

use Psr\Log\LoggerInterface;
use Doctrine\Common\Annotations\AnnotationReader;

use d84\Slim\Annotation\Controller as ControllerAnnotation;
use d84\Slim\Annotation\Middleware as MiddlewareAnnotation;

use d84\Slim\Annotation\ApiDoc\ApiDoc as ApiDocAnnotation;
use d84\Slim\Annotation\ApiDoc\Author as AuthorAnnotation;
use d84\Slim\Annotation\ApiDoc\Version as VersionAnnotation;
use d84\Slim\Annotation\ApiDoc\Deprecated as DeprecatedAnnotation;
use d84\Slim\Annotation\ApiDoc\Since as SinceAnnotation;

use d84\Slim\Annotation\Route\Route as RouteAnnotation;
use d84\Slim\Annotation\Route\Get as GetRouteAnnotation;
use d84\Slim\Annotation\Route\Post as PostRouteAnnotation;
use d84\Slim\Annotation\Route\Delete as DeleteRouteAnnotation;
use d84\Slim\Annotation\Route\Put as PutRouteAnnotation;
use d84\Slim\Annotation\Route\Patch as PatchRouteAnnotation;
use d84\Slim\Annotation\Route\Options as OptionsRouteAnnotation;
use d84\Slim\Annotation\Route\Any as AnyRouteAnnotation;
use d84\Slim\Annotation\Route\Map as MapRouteAnnotation;

class ControllerProcessor extends AbstractProcessor
{
    public function __construct(AnnotationReader $reader, LoggerInterface $logger)
    {
        parent::__construct($reader, $logger);
    }

    /**
     * @param array $annotations
     * @return array|bool
     */
    protected function doProcessClassAnnotations(array $annotations)
    {
        $controller_annotations = $this->findFirst($annotations, ControllerAnnotation::class);

        if (is_null($controller_annotations)) {
            return false; // Stop process the class
        }

        return [
          'controller' => $controller_annotations,
          'apidoc'     => $this->getApiDoc($annotations),
          'middleware' => $this->findAll($annotations, MiddlewareAnnotation::class)
        ];
    }

    /**
     * @param array $annotations
     * @return array|bool
     */
    protected function doProcessMethodAnnotations(array $annotations)
    {
        $routesanno = [
          RouteAnnotation::class,
          GetRouteAnnotation::class,
          PostRouteAnnotation::class,
          PutRouteAnnotation::class,
          PatchRouteAnnotation::class,
          DeleteRouteAnnotation::class,
          OptionsRouteAnnotation::class,
          AnyRouteAnnotation::class,
          MapRouteAnnotation::class
        ];

        $route_annotation = $this->findFirstOf($annotations, $routesanno);
        if (is_null($route_annotation)) {
            return [];
        }

        return [
          'route'      => $route_annotation,
          'apidoc'     => $this->getApiDoc($annotations),
          'middleware' => $this->findAll($annotations, MiddlewareAnnotation::class)
        ];
    }

    /**
     * @param array $annotations
     * @return array|bool
     */
    protected function doProcessPropertyAnnotations(array $annotations)
    {
        return [];
    }

    /**
     * @param  array  $annotations
     * @return array []
     *                ['author']     AuthorAnnotation
     *                ['version']    VersionAnnotation
     *                ['since']      SinceAnnotation
     *                ['deprecated'] bool
     */
    private function getApiDoc(array $annotations)
    {
        $author     = $this->ensureWithArgs($annotations, AuthorAnnotation::class, ['value' => []]);
        $version    = $this->ensureWithArgs($annotations, VersionAnnotation::class, ['value' => []]);
        $since      = $this->ensureWithArgs($annotations, SinceAnnotation::class, ['value' => []]);
        $deprecated = $this->findFirst($annotations, DeprecatedAnnotation::class) !== null ? true : false;

        return [
          'author'     => $author,
          'version'    => $version,
          'since'      => $since,
          'deprecated' => $deprecated
        ];
    }
}
