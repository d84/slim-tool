<?php
namespace d84\SlimCli\Annotation;

use Symfony\Component\Finder\Finder;
use Doctrine\Common\Annotations\AnnotationRegistry;

class Scanner
{
    /** @var AbstractProcessor */
    private $processor;

    /**
     * @param string            $autoload
     * @param AbstractProcessor $processor
     *
     * @throws \RuntimeException
     */
    public function __construct(string $autoload, AbstractProcessor $processor)
    {
        if (!file_exists($autoload)) {
            throw new \RuntimeException(
              "Not found '$autoload', perhaps you forgot to perform 'composer install' command"
            );
        }

        $loader = require($autoload);
        AnnotationRegistry::registerLoader([$loader, 'loadClass']);

        $this->processor = $processor;
    }

    /**
     * @param  string $dir
     * @return array
     */
    public function scan(string $dir): array
    {
        $finder = new Finder();
        $finder->ignoreDotFiles(true);
        $finder->size('>0K');
        $finder->in($dir)->files()->name('*.php');

        $map = [];
        $counters = [
          'total'     => 0,
          'processed' => 0
        ];

        foreach ($finder as $file) {
            $result = $this->processor->process($file);
            if ($result !== false) {
                $map[] = $result;
                $counters['processed']++;
            }
            $counters['total']++;
        }

        return [
          'map'    => $map,
          'report' => $counters
        ];
    }
}
